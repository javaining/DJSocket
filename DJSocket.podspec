#
# Be sure to run `pod lib lint DJSocket.podspec' to ensure this is a
# valid spec before submitting.
#
# Any lines starting with a # are optional, but their use is encouraged
# To learn more about a Podspec see http://guides.cocoapods.org/syntax/podspec.html
#

Pod::Spec.new do |s|
  s.name             = 'DJSocket'
  s.version          = '1.0.3'
  s.summary          = '网络库'

# This description is used to generate tags and improve search results.
#   * Think: What does it do? Why did you write it? What is the focus?
#   * Try to keep it short, snappy and to the point.
#   * Write the description between the DESC delimiters below.
#   * Finally, don't worry about the indent, CocoaPods strips it!

  s.description      = 'socket 网络库'

  s.homepage         = 'https://github.com/dujia/DJSocket'
  s.license          = { :type => 'MIT', :file => 'LICENSE' }
  s.author           = { 'dujia' => 'zzj19910517@gmail.com' }
  s.source           = { :git => 'https://git.oschina.net/jia/DJSocket.git', :tag => s.version.to_s, :submodules => true}
  # s.social_media_url = 'https://twitter.com/<TWITTER_USERNAME>'

  s.ios.deployment_target = '8.0'

  s.source_files = ['DJSocket/Classes/**/*','DJSocket/libnetwork/libnetwork/src/*','DJSocket/libnetwork/libnetwork/include/*']
  s.libraries = 'c++'
  s.requires_arc = true

  # s.public_header_files = 'Pod/Classes/**/*.h'
  # s.frameworks = 'UIKit', 'MapKit'
  # s.dependency 'AFNetworking', '~> 2.3'
end
