# DJSocket

[![CI Status](http://img.shields.io/travis/dujia/DJSocket.svg?style=flat)](https://travis-ci.org/dujia/DJSocket)
[![Version](https://img.shields.io/cocoapods/v/DJSocket.svg?style=flat)](http://cocoapods.org/pods/DJSocket)
[![License](https://img.shields.io/cocoapods/l/DJSocket.svg?style=flat)](http://cocoapods.org/pods/DJSocket)
[![Platform](https://img.shields.io/cocoapods/p/DJSocket.svg?style=flat)](http://cocoapods.org/pods/DJSocket)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

DJSocket is available through [CocoaPods](http://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod "DJSocket"
```

## Author

dujia, dujia@mogujie.con

## License

DJSocket is available under the MIT license. See the LICENSE file for more info.
