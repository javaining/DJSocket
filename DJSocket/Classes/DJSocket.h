//
//  DJSocket.h
//  Pods
//
//  Created by zhenjia zhang on 2017/7/16.
//
//

#import <Foundation/Foundation.h>
#import "DJSocketCallBackProtocol.h"

typedef NS_ENUM(NSUInteger, DJSocketConnectionStatus) {
    DJSocketConnectionStatusConnected = 0x00,
    DJSocketConnectionStatusClosed = 0x01,
    DJSocketConnectionStatusConnecting = 0x02,
    DJSocketConnectionStatusInvalidHandle = 0x03,
};


typedef NS_ENUM(NSUInteger, DJSocketConnectionCloseReason) {
    DJSocketConnectionCloseReasonTimeout = 1,
    DJSocketConnectionCloseReasonConnecting = 3,
    DJSocketConnectionCloseReasonException = 4,
    DJSocketConnectionCloseReasonClose = 5,
    DJSocketConnectionCloseReasonSys = 6,
};

@interface DJSocket : NSObject
@property (nonatomic, readonly)NSString *serverIP;
@property (nonatomic, readonly)NSInteger port;

+ (instancetype)shareInstance;

- (void)configWithIP:(NSString *)ip port:(NSInteger)port delegate:(id<DJSocketCallBackProtocol>) delegate;

- (BOOL)connect;

- (void)close;

- (BOOL)sendData:(NSData *)data;

- (DJSocketConnectionStatus)status;

@end
