//
//  DJNetWork.h
//  Pods
//
//  Created by zhenjia zhang on 2017/7/16.
//
//

#ifndef DJNetWork_h
#define DJNetWork_h

#import <Network.h>
#import <DJSocketCallBackProtocol.h>

class DJNetwork: public libnetwork::NetWork {

public:
    void setNetWorkDelegate(id<DJSocketCallBackProtocol> s);
    virtual void onConnect(int handle);
    virtual void onRead(int handle, const char *buf, int len);
    virtual void onClose(int handle, int reason);
    
private:
    __weak id<DJSocketCallBackProtocol> _delegate;
};

void DJNetwork::setNetWorkDelegate(id<DJSocketCallBackProtocol> s)
{
    _delegate = s;
}

void DJNetwork::onConnect(int handle)
{
    [_delegate onConnect:handle];
}

void DJNetwork::onRead(int handle, const char *buf, int len)
{
    NSData *pdu_data = [[NSData alloc] initWithBytes:buf length:len];
    [_delegate onRead:handle data:pdu_data];
}

void DJNetwork::onClose(int handle, int reason)
{
    [_delegate onClose:handle reason:reason];
}

#endif /* DJNetWork_h */
