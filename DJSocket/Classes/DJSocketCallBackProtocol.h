//
//  DJSocketCallBackProtocol.h
//  Pods
//
//  Created by zhenjia zhang on 2017/7/16.
//
//

#import <Foundation/Foundation.h>

@protocol DJSocketCallBackProtocol <NSObject>

- (void)onConnect:(int)connectionID;

- (void)onRead:(int)connectionID data:(NSData *)data;

- (void)onClose:(int)connectionID reason:(int)reason;

@end
