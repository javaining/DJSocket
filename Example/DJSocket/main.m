//
//  main.m
//  DJSocket
//
//  Created by dujia on 07/15/2017.
//  Copyright (c) 2017 dujia. All rights reserved.
//

@import UIKit;
#import "DJAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([DJAppDelegate class]));
    }
}
