//
//  DJViewController.m
//  DJSocket
//
//  Created by dujia on 07/15/2017.
//  Copyright (c) 2017 dujia. All rights reserved.
//

#import "DJViewController.h"
#import <DJSocket/DJSocket.h>

@interface DJViewController ()<DJSocketCallBackProtocol>

@end

@implementation DJViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
    [[DJSocket shareInstance] configWithIP:@"139.196.190.127" port:8886 delegate:self];
    [[DJSocket shareInstance] connect];
	// Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark -
#pragma mark - DJSocketCallBackProtocol

- (void)onConnect:(int)connectionID {

}

- (void)onRead:(int)connectionID data:(NSData *)data {

}

- (void)onClose:(int)connectionID reason:(int)reason {

}

@end
