//
//  DJAppDelegate.h
//  DJSocket
//
//  Created by dujia on 07/15/2017.
//  Copyright (c) 2017 dujia. All rights reserved.
//

@import UIKit;

@interface DJAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
